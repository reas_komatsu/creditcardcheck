// クレカ番号フォームのフォーカス移動
function nextFocus(elm, max) {
    var next_elm = eval(elm+1);
    if (document.zip.elements[elm].value.length >= max) {
        document.zip.elements[next_elm].focus();
        console.log("フォーカス移動しました");
    }
}

$(".required").blur(function(){
    if($(this).val() == ""){
        $(this).siblings('span.error-required').text("※必須");
        $(this).addClass("errored");
        console.log("必須項目出してます");
    } else {
        $(this).siblings('span.error-required').text("");
        $(this).removeClass("errored");
        console.log("必須項目削除");
    }
});

var selectchild = $('.required').parent('.dropdown');
var selectparent = $(selectchild).parent('.select-inner');
var selecthoner = $(selectparent).siblings('p');

$("#select").blur(function(){
    if($(this).val() == ""){
        $(selecthoner).children('span.error-required').text("※必須");
        $(this).addClass("errored");
        console.log("必須項目出してます");
    } else {
        $(selecthoner).children('span.error-required').text("");
        $(this).removeClass("errored");
        console.log("必須項目wo削除");
    }
});

$(".credit-number").keyup (function(){
    if(!$(this).val().match(/[0-9]/)){
        $(this).siblings('span.error-required').text("※正しく入力してください");
        $(this).addClass("errored");
        console.log("番号が間違ってます");
    } else {
        $(this).siblings('span.error-required').text("");
        $(this).removeClass("errored");
        console.log("クレカエラー削除");
    }
});

$(".name").keyup(function(){
    if(!$(this).val().match(/[A-Z a-z]/)){
        $(this).siblings('span.error-required').text("※正しく入力してください");
        $(this).addClass("errored");
        console.log("名前が違います");
    } else {
        $(this).siblings('span.error-required').text("");
        $(this).removeClass("errored");
        console.log("名前エラー消しました");
    }
});

$("#security-code").keyup(function(){
    if(!$(this).val().match(/[0-9]/)){
        $(this).siblings('span.error-required').text("※正しく入力してください");
        $(this).addClass("errored");
        console.log("セキュリティコード間違い");
    } else {
        $(this).siblings('span.error-required').text("");
        $(this).removeClass("errored");
        console.log("セキュリティコードエラー消しました");
    }
});

//送信時の必須項目入力チェック
$("#submit-input").on('click touchend',function(){
    $(".required").each(function(){
        if($(this).val() == ""){
            $(selecthoner).children('span.error-required').text("※必須");
            $(this).siblings('span.error-required').text("※必須");
            $(this).addClass("errored");
            console.log("記入漏れあり");
        }
    });
    if($(".errored").length){
        return false;
        console.log("情報送信しません");
    }
});

